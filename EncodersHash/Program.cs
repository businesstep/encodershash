﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace EncodersHash
{
    public static class Base64Hash
    {
        public static string EncodeToBase64(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return null;
            }

            Byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(s);

            return Convert.ToBase64String(toEncodeAsBytes);
        }

        public static string DecodeFromBase64(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return null;
            }

            Byte[] encodedDataAsBytes = Convert.FromBase64String(s);

            return ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
        }
    }

    public static class SHA256Hash
    {
        public static string GetSHA256Hash(SHA256 sha256, string s)
        {
            byte[] buffer = sha256.ComputeHash(Encoding.UTF8.GetBytes(s)/*ASCIIEncoding.ASCII.GetBytes(s)*/);

            StringBuilder sBuilder = new StringBuilder();

            foreach (byte bt in buffer)
            {
                sBuilder.Append(bt.ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static string EncodeToSHA256(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return null;
            }

            using (SHA256 sha256 = SHA256.Create())
            {
                return GetSHA256Hash(sha256, s).ToUpper();
            }

        }
    }

    public static class Md5Hash
    {
        public static string GetMd5Hash(MD5 md5Hash, string s)
        {
            byte[] buffer = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(s));

            StringBuilder sBuilder = new StringBuilder();

            foreach (byte bt in buffer)
            {
                sBuilder.Append(bt.ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static string ToMD5(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return null;
            }

            using (MD5 md5Hash = MD5.Create())
            {
                return GetMd5Hash(md5Hash, s);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Encoders Hash\n");

            string ordernum = "1234";
            string code     = "pl";
            string secret   = "a4bcaDX6";

            string sha256 = SHA256Hash.EncodeToSHA256(ordernum + secret);
            string base64 = Base64Hash.EncodeToBase64(code + ":" + sha256);

            Console.WriteLine("sha256 of {0} = {1}", ordernum + secret, sha256);
            Console.WriteLine("encode base64 = {0}", base64);
 
            Console.ReadLine();
        }
    }
}
